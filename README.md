# Sobki

This repository is the main entrypoint of the Sobki ecosystem and for the common
documentation parts of the different profiles.

See the [project page](https://www.drupal.org/project/sobki) and the
[documentation pages](https://project.pages.drupalcode.org/sobki) for detailed
information.
