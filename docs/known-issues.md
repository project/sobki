# Known issues


## Not possible to delete a block content type after usage in Layout Builder

Not possible to delete a block content type because there are block content
blocks remaining.

Layout Builder keeps track of inline block content blocks usage in entities.
When an inline block is no more used, it is deleted during the next CRON
execution.

But currently Layout Builder does not handle revisions and translations. So you
can only remove an inline block by deleting its parent entity.

There is a [core issue](https://www.drupal.org/project/drupal/issues/3042418)
for that. We currently have not applied the patch proposed in the issue because
it alters the database structure, so if the patch is never merged in core it
can become hard to maintain.
