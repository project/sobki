---
hide:
  - toc
---

# Sobki

The Sobki profiles aim at providing an enhanced editor experience based on the
[UI Suite design system approach](https://www.drupal.org/project/ui_suite) to
lower the need of additional code on top of contributed modules and empower
site builders.


## Profiles

Currently, there are two profiles being worked on:

- [Sobki Bootstrap](https://www.drupal.org/project/sobki_profile_bootstrap)
- [Sobki DSFR](https://www.drupal.org/project/sobki_profile_dsfr)


## Themes

The common administration theme is
[Sobki Admin Theme](https://www.drupal.org/project/sobki_theme_admin).
