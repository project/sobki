# Installation


## Install the website

Finally, when all your dependencies are properly setup, be sure to have a
working setting file with the right database credentials and run one of the
following commands regarding which sobki profile you want to use.


### Sobki Bootstrap

```sh
drush site:install sobki_profile_bootstrap --account-name=admin --account-pass=admin -y
```

### Sobki DSFR

```sh
drush site:install sobki_profile_dsfr --account-name=admin --account-pass=admin -y
```
