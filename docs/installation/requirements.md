# Requirements

The Sobki profiles has no specific system dependency.


## Core

It has the same requirements has
[Drupal Core](https://www.drupal.org/docs/getting-started/system-requirements):

- [Web server](https://www.drupal.org/docs/getting-started/system-requirements/web-server-requirements)
- [Database](https://www.drupal.org/docs/getting-started/system-requirements/database-server-requirements)
- [PHP](https://www.drupal.org/docs/getting-started/system-requirements/php-requirements)


## Caching layers

It is greatly advised to add a backend cache layer (Redis, Memcache, other) and
a frontend cache layer (Varnish, CDN, other).

The profiles are not opinionated to the caching solutions a website may use
on its hosting. Therefore, it does not provide any system related modules
or configuration.

If some specificities exist, it should have been documented in the README.md
file of the appropriate module.
