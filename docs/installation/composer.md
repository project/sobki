# Composer


## Start with a composer.json template

The `composer.json` file of a project using Sobki should have the
following content, in addition to other content specific to the project.

In the following example, `app` is the root folder of Drupal. The name can have
a different value.

```json
{
  "require": {
    "composer/installers": "^2.0",
    "cweagans/composer-patches": "^1.0",
    "oomphinc/composer-installers-extender": "^2.0"
  },
  "repositories": {
    "asset-packagist": {
      "type": "composer",
      "url": "https://asset-packagist.org"
    },
    "drupal": {
      "type": "composer",
      "url": "https://packages.drupal.org/8"
    }
  },
  "minimum-stability": "dev",
  "prefer-stable": true,
  "extra": {
    "composer-exit-on-patch-failure": true,
    "drupal-scaffold": {
      "file-mapping": {
        "[web-root]/robots.txt": false
      },
      "locations": {
        "web-root": "./app"
      }
    },
    "enable-patching": true,
    "installer-paths": {
      "app/core": [
        "type:drupal-core"
      ],
      "app/libraries/fontawesome": [
        "npm-asset/fortawesome--fontawesome-free"
      ],
      "app/libraries/{$name}": [
        "type:drupal-library",
        "type:bower-asset",
        "type:npm-asset"
      ],
      "app/modules/contrib/{$name}": [
        "type:drupal-module"
      ],
      "app/modules/custom/{$name}": [
        "type:drupal-custom-module"
      ],
      "app/profiles/contrib/{$name}": [
        "type:drupal-profile"
      ],
      "app/profiles/custom/{$name}": [
        "type:drupal-custom-profile"
      ],
      "app/themes/contrib/{$name}": [
        "type:drupal-theme"
      ],
      "app/themes/custom/{$name}": [
        "type:drupal-custom-theme"
      ],
      "drush/Commands/contrib/{$name}": [
        "type:drupal-drush"
      ]
    },
    "installer-types": [
      "bower-asset",
      "npm-asset"
    ],
    "patchLevel": {
      "drupal/core": "-p2"
    }
  }
}
```

See
[Drupal.org documentation](https://www.drupal.org/docs/develop/using-composer/manage-dependencies)
for detailed explanations.


## Mandatory dependencies

Once the `composer.json` file is set up, run one of the following command
regarding which Sobki profile you want to use.


### Sobki Bootstrap

```sh
composer require drupal/sobki_profile_bootstrap
```

### Sobki DSFR

```sh
composer require drupal/sobki_profile_dsfr
```

## Other dependencies

There are other Composer packages not mandatory for Sobki, but these may
be useful.


### Drush

```sh
composer require drush/drush
```

### Drupal Paranoia

We strongly recommend the usage of [Drupal Paranoia](https://github.com/drupal-composer/drupal-paranoia)
Composer plugin to provide an additional security layer.

Modify your `composer.json` file with the proper `extra` declarations (see
[Drupal Paranoia installation process](https://github.com/drupal-composer/drupal-paranoia?tab=readme-ov-file#installation)) and run the following command:

```sh
composer require drupal-composer/drupal-paranoia
```

### Drupal Localization

If you want to automatically download Drupal projects translations when adding
them through Composer, consider using [Drupal Localization](https://github.com/drupal-composer/drupal-l10n).

Modify your `composer.json` file with the proper `extra` declarations (see
[Drupal Localization configuration process](https://github.com/drupal-composer/drupal-l10n?tab=readme-ov-file#configuration)) and run the following command:

```sh
composer require drupal-composer/drupal-l10n
```

### Asset Packagist

To retrieve non-PHP libraries, we use
[Asset Packagist](https://asset-packagist.org).

If you do not want to use Asset Packagist, you can declare the packages manually
following
[Composer documentation](https://getcomposer.org/doc/05-repositories.md).
