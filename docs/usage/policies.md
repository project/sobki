# Policies


## Composer policy

The libraries, PHP or assets, required by the profiles are on fixed version.


## Configuration policy

The Sobki profiles provides configuration on installation.

**Configuration updates will not be handled.** Except if this comes,
for example, from a change in the configuration structure of a Sobki plugin
like a field formatter.


## Multilingual policy

The profiles come in English for either code and configuration.

By default, English and French languages are enabled.


## Versioning

The profiles branches are named with the following nomenclature
`[core major version].[Sobki minor/build].[Sobki bug fix/run]`.
