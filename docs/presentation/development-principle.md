# Development principle

Sobki leans as much as possible on Drupal core and contrib modules.

If a feature does not exist in contrib, we either:

1. try to patch core or existing modules to improve existing projects
1. create a new contrib module so non Sobki projects can also use it
1. if not possible to have something generic enough to put in contrib, create a
   new "custom" module in Sobki which can be used as POC until put into existing
   contrib projects
