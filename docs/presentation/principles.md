# 3 Principles


## 1 - Design system and no-code approach

Sobki uses design systems with modules and themes from the
[UI Suite](https://drupal.org/project/ui_suite) ecosystem.

**By leveraging site building capabilities**, this allows to have a codebase
decoupled from the data structure and this empowers administrators.

This allows to build multiple cohesive and branded websites effortlessly with a
high code quality.

> Note: Currently, as not every part of Drupal are site buildable with control
> on the rendering, it is more a "low-code" approach even if we aim for a
> "no-code" approach.


## 2 - Improved contribution experience and UI

Having an improved contribution experience is a key to a website adoption by
editorial teams. That's why we made efforts in this direction too:

* [Gin](https://www.drupal.org/project/gin) and its ecosystem is used as admin
  theme in all Sobki's profiles for consistency.
* Layout Builder's UI has been improved and modules of its ecosystem are used.
* Content editing modules are present to enhance RTE or content modeling.

Also, as configuration is time-consuming, **Sobki is ready out-of-the-box**.
Examples of available features (or on the roadmap):

* Default block types (text, media, map, form, carousel, etc.)
* Default content types (landing page, news, blog post, etc.)
* Media management improvements
* Configured SEO modules
* Configured roles and permissions
* Configured publication workflow
* Configured webforms
* Configured security modules
* AI integration
* Default demo content

> Note: Just like Drupal, Sobki is limitless; you can also use it as a base and
> add any Drupal modules you want.


## 3 - Sustainable IT

Sobki aims to address the pillars of sustainable IT principles:

* **Environmental:** a reusable codebase for each of your sites, streamlined
  features, and a continuous improvement approach to optimizing Sobki in terms
  of energy consumption.
* **Social:** special attention is paid to **current standards**, both from an
  accessibility (WCAG, RGAA) and privacy (GDPR) perspectives.
* **Governance:** an open source approach that **protects you from potential
  vendor lock-in** and regular **security audits** on Sobki's codebase.
