# Testimonials

Here is a list of testimonials from Sobki's users:

> Using Sobki has saved me time on my project. With the Sobki installation
> profile, we don't need to manage the UI suite module ecosystem and all the
> patches to use. Starting the initialization of a Drupal site with a solid base
> is very satisfying at the project launch.
>
> **Germain ESCRIVA, tech lead**

> Creating content on Drupal has become child's play with Sobki. The interface
> is intuitive and the template functionality allows creating content in two
> minutes without coding while maintaining graphical coherence.
>
> **A.L., project manager**
