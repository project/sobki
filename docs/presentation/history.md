# History


## Why this name?

The story behind the name `Sobki` is the result of a thoughtful creative
process. During several branding workshops, the team wanted a name that hinted
at its **origins** without directly describing the product. The name also had
to be **simple** and **accessible**.

Inspired by [Drupal’s iconic water drop](https://www.drupal.org/about/history),
the team chose a **water theme** to symbolize **creation and sharing**, two core
pillars of the solution.

The discussions led to `Sobek`, the Egyptian god of water and fertility, a
fitting symbol for Sobki's **ability to create and lay the foundation for Drupal
sites**. Sobek, with his many scales, also represented **the modularity of the
solution**.

However, we sought a **softer sound** which brought us to `Sobki`, an existing
variation of Sobek. This name captures the essence of the solution while
remaining modern and accessible.

And... no, it’s not related to the
[Polish village](https://fr.wikipedia.org/wiki/Sobki_(%C5%81%C3%B3d%C5%BA)) of
the same name. 😜
